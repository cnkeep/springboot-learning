package com.cnkeep.springboot.chapter1.section1.domain.entity;

import java.io.Serializable;

/**
 * 描述~
 *
 * @author <a href="1348555156@qq.com">LeiLi.Zhang</a>
 * @version 0.0.0
 * @date 2018/5/18
 */
public interface Entity extends Serializable{
    Serializable getId();
}
